package Data;

import Business.MenuItem;

import java.io.*;
import java.util.List;

public class RestaurantSerializer
{
    public static void serialize(List<MenuItem> menuItems)
    {
        try
        {
            FileOutputStream f = new FileOutputStream("menu.ser");
            ObjectOutputStream out = new ObjectOutputStream(f);

            out.writeObject(menuItems);
            out.close();
            f.close();
        }

        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public static List<MenuItem> deserialize()
    {
        List<MenuItem> menuItems = null;

        try
        {
            FileInputStream f = new FileInputStream("menu.ser");
            ObjectInputStream in = new ObjectInputStream(f);

            menuItems = (List<MenuItem>) in.readObject();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }

        return menuItems;
    }
}
