package Data;

import Business.MenuItem;
import Business.Order;
import Business.Restaurant;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.List;

public class FileWriter
{
    public static void createReceipt(Order o, float price, List<MenuItem> items)
    {
        SimpleDateFormat sdf = new SimpleDateFormat("MM-dd_HH:mm:ss");

        String fileLocation = "./Order_" + o.getId() + "_Table_"+ o.getTable() + "_" + sdf.format(o.getDateOrdered()) + ".pdf";
        Document doc = new Document();

        try
        {
            PdfWriter.getInstance(doc, new FileOutputStream(new File(fileLocation)));

            doc.open();

            Font titleFont = new Font(Font.FontFamily.TIMES_ROMAN, 24, Font.BOLD);
            Font h1Font = new Font(Font.FontFamily.TIMES_ROMAN, 14, Font.BOLD);
            Font textFont = new Font(Font.FontFamily.TIMES_ROMAN, 12);

            Paragraph titlep = new Paragraph();
            titlep.setAlignment(Element.ALIGN_CENTER);
            titlep.setSpacingAfter(24.0f);
            titlep.setFont(titleFont);
            titlep.add("Order");

            Paragraph order = new Paragraph();
            order.setAlignment(Element.ALIGN_LEFT);
            order.setFont(h1Font);
            order.setSpacingAfter(15.0f);
            order.add("Details: ");

            Paragraph orderDetails = new Paragraph();
            orderDetails.setFont(textFont);
            orderDetails.setSpacingAfter(15.0f);
            orderDetails.setAlignment(Element.ALIGN_LEFT);
            orderDetails.add("Id: " + o.getId() + "\n");
            orderDetails.add("Table: " + o.getTable() + "\n");
            orderDetails.add("Date: " + sdf.format(o.getDateOrdered()) + "\n");

            Paragraph itemsP = new Paragraph();
            itemsP.setFont(h1Font);
            itemsP.setAlignment(Element.ALIGN_LEFT);
            itemsP.setSpacingAfter(15.0f);
            itemsP.add("Items:");

            Paragraph itemDetailsP = new Paragraph();
            itemDetailsP.setFont(textFont);
            itemDetailsP.setAlignment(Element.ALIGN_LEFT);
            itemDetailsP.setSpacingAfter(10.0f);

            items.forEach(e -> itemDetailsP.add(e.toStringTree(0, true)));

            Paragraph total = new Paragraph();
            total.setAlignment(Element.ALIGN_LEFT);
            total.setSpacingBefore(15.0f);
            total.setFont(h1Font);
            total.setSpacingAfter(15.0f);
            total.add("Total:");

            Paragraph totalPrice = new Paragraph();
            totalPrice.setAlignment(Element.ALIGN_LEFT);
            totalPrice.setFont(textFont);
            totalPrice.add(Float.toString(price));

            doc.add(titlep);
            doc.add(order);
            doc.add(orderDetails);
            doc.add(itemsP);
            doc.add(itemDetailsP);
            doc.add(total);
            doc.add(totalPrice);

            doc.close();
        }

        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
