package Presentation;

import Business.CompositeProduct;
import Business.IRestaurantProcessing;
import Business.MenuItem;
import Business.Restaurant;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class ChildEditWindow extends JFrame
{
    List<MenuItem> items;
    AdministratorGUI adminGUI;

    IRestaurantProcessing restaurantProcessing;

    private JButton addBtn;
    private JButton editBtn;
    private JButton deleteBtn;
    private JButton editChildBtn;

    private JPanel tablePanel;
    private JPanel editPanel;

    private JTextField id;
    private JTextField name;
    private JTextField price;
    private JCheckBox type;

    JTable table;

    public ChildEditWindow(List<MenuItem> items, AdministratorGUI adminGUI)
    {
        restaurantProcessing = Restaurant.getInstance();
        this.items = items;
        this.adminGUI = adminGUI;

        tablePanel = new JPanel();
        editPanel = new JPanel(new GridLayout(5, 1));

        addBtn = new JButton("Add");
        editBtn = new JButton("Edit");
        deleteBtn = new JButton("Delete");
        editChildBtn = new JButton("Edit child");

        id = new JTextField(20);
        name = new JTextField(20);
        price = new JTextField(20);
        type = new JCheckBox("Composite");

        Container idCont = new Container();
        Container nameCont = new Container();
        Container priceCont = new Container();
        Container typeCont = new Container();
        Container buttons = new Container();

        idCont.setLayout(new FlowLayout());
        nameCont.setLayout(new FlowLayout());
        priceCont.setLayout(new FlowLayout());
        typeCont.setLayout(new FlowLayout());
        buttons.setLayout(new FlowLayout());

        idCont.add(new JLabel("id: "));
        idCont.add(id);
        nameCont.add(new JLabel("Name: "));
        nameCont.add(name);
        priceCont.add(new JLabel("Price: "));
        priceCont.add(price);
        buttons.add(addBtn);
        buttons.add(editBtn);
        buttons.add(editChildBtn);
        buttons.add(deleteBtn);
        typeCont.add(type);

        editPanel.add(idCont);
        editPanel.add(nameCont);
        editPanel.add(priceCont);
        editPanel.add(typeCont);
        editPanel.add(buttons);

        addBtn.addActionListener(e ->
        {
            MenuItem toAdd = restaurantProcessing.createMenuItem(name.getText(), Float.parseFloat(price.getText()), type.isSelected());
            restaurantProcessing.addMenuItem(this.items, toAdd);
            refreshTable();
        });

        editBtn.addActionListener(e ->
        {
            MenuItem toAdd = restaurantProcessing.createMenuItem(name.getText(), Float.parseFloat(price.getText()), type.isSelected());
            restaurantProcessing.editMenuItem(this.items, Integer.parseInt(id.getText()), toAdd);
            refreshTable();
        });

        deleteBtn.addActionListener(e ->
        {
            restaurantProcessing.deleteMenuItem(this.items, Integer.parseInt(id.getText()));
            this.adminGUI.refreshTable();
            refreshTable();
        });

        editChildBtn.addActionListener(e ->
        {
            createChildEditWindow(Integer.parseInt(id.getText()));
        });

        table = new JTable();
        table.setModel(restaurantProcessing.childTable(this.items));

        tablePanel.add(new JScrollPane(table));

        JTabbedPane panes = new JTabbedPane();

        panes.addTab("Table", tablePanel);
        panes.addTab("Edit", editPanel);

        this.add(panes);
        this.setTitle("Edit child");
        this.setSize(600, 400);
        this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }

    public void refreshTable()
    {
        table.setModel(restaurantProcessing.childTable(this.items));

        this.adminGUI.refreshTable();
    }

    private void createChildEditWindow(int id)
    {
        MenuItem selected = items.get(id);

        if(selected.getClass().getSimpleName().equalsIgnoreCase("BaseProduct"))
        {
            JOptionPane.showMessageDialog(this, "Object is leaf node", "Error", JOptionPane.ERROR_MESSAGE);
        }

        else
        {
            CompositeProduct newEditPanel = (CompositeProduct) selected;

            new ChildEditWindow(newEditPanel.getChildMenuItems(), this.adminGUI);
        }
    }
}
