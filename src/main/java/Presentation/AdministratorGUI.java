package Presentation;

import Business.CompositeProduct;
import Business.IRestaurantProcessing;
import Business.MenuItem;
import Business.Restaurant;

import javax.swing.*;
import java.awt.*;
import java.util.List;

public class AdministratorGUI extends JFrame
{
    IRestaurantProcessing restaurantProcessing;

    private JButton addBtn;
    private JButton editBtn;
    private JButton deleteBtn;
    private JButton editChildBtn;

    private JPanel tablePanel;
    private JPanel editPanel;

    private JTextField id;
    private JTextField name;
    private JTextField price;
    private JCheckBox type;

    JTable table;

    public AdministratorGUI()
    {
        super();
        restaurantProcessing = Restaurant.getInstance();

        tablePanel = new JPanel();
        editPanel = new JPanel(new GridLayout(5, 1));

        addBtn = new JButton("Add");
        editBtn = new JButton("Edit");
        deleteBtn = new JButton("Delete");
        editChildBtn = new JButton("Edit child");

        id = new JTextField(20);
        name = new JTextField(20);
        price = new JTextField(20);
        type = new JCheckBox("Composite");

        Container idCont = new Container();
        Container nameCont = new Container();
        Container priceCont = new Container();
        Container typeCont = new Container();
        Container buttons = new Container();

        idCont.setLayout(new FlowLayout());
        nameCont.setLayout(new FlowLayout());
        priceCont.setLayout(new FlowLayout());
        typeCont.setLayout(new FlowLayout());
        buttons.setLayout(new FlowLayout());

        idCont.add(new JLabel("id: "));
        idCont.add(id);
        nameCont.add(new JLabel("Name: "));
        nameCont.add(name);
        priceCont.add(new JLabel("Price: "));
        priceCont.add(price);
        buttons.add(addBtn);
        buttons.add(editBtn);
        buttons.add(editChildBtn);
        buttons.add(deleteBtn);
        typeCont.add(type);

        editPanel.add(idCont);
        editPanel.add(nameCont);
        editPanel.add(priceCont);
        editPanel.add(typeCont);
        editPanel.add(buttons);

        addBtn.addActionListener(e ->
        {
            MenuItem toAdd = restaurantProcessing.createMenuItem(name.getText(), Float.parseFloat(price.getText()), type.isSelected());
            restaurantProcessing.addMenuItem(restaurantProcessing.getMenu(), toAdd);
            refreshTable();
        });

        editBtn.addActionListener(e ->
        {
            MenuItem toAdd = restaurantProcessing.createMenuItem(name.getText(), Float.parseFloat(price.getText()), type.isSelected());
            restaurantProcessing.editMenuItem(restaurantProcessing.getMenu(), Integer.parseInt(id.getText()), toAdd);
            refreshTable();
        });

        deleteBtn.addActionListener(e ->
        {
            restaurantProcessing.deleteMenuItem(restaurantProcessing.getMenu(), Integer.parseInt(id.getText()));
            refreshTable();
        });

        editChildBtn.addActionListener(e ->
        {
            createChildEditWindow(Integer.parseInt(id.getText()));
        });

        table = new JTable();
        table.setModel(restaurantProcessing.getTable(0));

        tablePanel.add(new JScrollPane(table));

        JTabbedPane panes = new JTabbedPane();

        panes.addTab("Table", tablePanel);
        panes.addTab("Edit", editPanel);

        this.add(panes);
        this.setTitle("Administrator");
        this.setSize(600, 400);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }

    private void createChildEditWindow(int id)
    {
        MenuItem selected = restaurantProcessing.getMenu().get(id);

        if(selected.getClass().getSimpleName().equalsIgnoreCase("BaseProduct"))
        {
            JOptionPane.showMessageDialog(this, "Object is leaf node", "Error", JOptionPane.ERROR_MESSAGE);
        }

        else
        {
            CompositeProduct newEditPanel = (CompositeProduct) selected;

            new ChildEditWindow(newEditPanel.getChildMenuItems(), this);
        }
    }

    public void refreshTable()
    {
        table.setModel(restaurantProcessing.getTable(0));
    }
}
