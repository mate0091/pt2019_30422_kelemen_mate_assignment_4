package Presentation;

import Business.MenuItem;

import javax.swing.*;
import java.awt.*;
import java.text.SimpleDateFormat;
import java.util.*;

public class ChefGUI extends JFrame implements Observer
{
    public JTextArea textArea;

    public ChefGUI()
    {
        textArea = new JTextArea(50, 50);

        JPanel panel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        panel.add(new JScrollPane(textArea, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED));

        this.add(panel);
        this.setSize(600, 400);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setTitle("Chef");
        this.setVisible(true);
    }

    @Override
    public void update(Observable o, Object arg)
    {
        StringBuilder sb = new StringBuilder();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");

        Collection<MenuItem> items = (Collection<MenuItem>) arg;
        sb.append(sdf.format(new Date(System.currentTimeMillis())) + "- Chef is cooking now\n");

        Iterator<MenuItem> it = items.iterator();

        while(it.hasNext())
        {
            sb.append(it.next().toString() + "\n");
        }

        sb.append("\n");

        textArea.append(sb.toString());
    }
}
