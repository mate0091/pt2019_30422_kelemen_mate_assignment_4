package Presentation;

import Business.IRestaurantProcessing;
import Business.MenuItem;
import Business.Order;
import Business.Restaurant;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class WaiterGUI extends JFrame
{
    private IRestaurantProcessing restaurantProcessing;

    private JPanel orderPanel;

    private JButton createOrderBtn;
    private JButton addBtn;
    private JButton placeOrderBtn;
    private JButton clearOrderBtn;
    private JButton computePriceBtn;

    private JTextField orderIdText;
    private JTextField tableIdText;
    private JTextField indexText;

    private JTextField priceTxt;

    private JTextArea menuItemsTxt;
    private Order order;

    private List<MenuItem> currentOrder;

    public WaiterGUI()
    {
        super();
        restaurantProcessing = Restaurant.getInstance();
        currentOrder = new ArrayList<>();

        addBtn = new JButton("Add menu item");
        placeOrderBtn = new JButton("Place order");
        clearOrderBtn = new JButton("Clear list");
        computePriceBtn = new JButton("Compute price");
        createOrderBtn = new JButton("Create order");

        orderIdText = new JTextField(30);
        tableIdText = new JTextField(30);
        indexText = new JTextField(30);

        menuItemsTxt = new JTextArea(20, 20);
        priceTxt = new JTextField(10);
        priceTxt.setEditable(false);

        Container orderCont = new Container();
        Container auxCont = new Container();
        Container btnCont = new Container();

        orderCont.setLayout(new FlowLayout(FlowLayout.CENTER));
        orderCont.add(new JLabel("Order id:                      "));
        orderCont.add(orderIdText);
        orderCont.add(new JLabel("Table id:                      "));
        orderCont.add(tableIdText);

        auxCont.setLayout(new FlowLayout(FlowLayout.CENTER));
        auxCont.add(new JLabel("Menu item index:                 "));
        auxCont.add(indexText);
        auxCont.add(new JLabel("Price:                           "));
        auxCont.add(priceTxt);

        btnCont.setLayout(new FlowLayout(FlowLayout.CENTER));
        btnCont.add(addBtn);
        btnCont.add(createOrderBtn);
        btnCont.add(clearOrderBtn);
        btnCont.add(computePriceBtn);
        btnCont.add(placeOrderBtn);

        orderPanel = new JPanel();
        orderPanel.setLayout(new GridLayout(4, 1));

        orderPanel.add(orderCont);
        orderPanel.add(auxCont);
        orderPanel.add(menuItemsTxt);
        orderPanel.add(btnCont);

        addBtn.addActionListener(e ->
        {
            addToList();
            refreshText();
        });

        placeOrderBtn.addActionListener(e -> createBill());
        clearOrderBtn.addActionListener(e ->
        {
            clearList();
            refreshText();
        });

        createOrderBtn.addActionListener(e -> createOrder());
        computePriceBtn.addActionListener(e -> computePrice());

        this.add(orderPanel);
        this.setTitle("Waiter");
        this.setSize(580, 400);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }

    private void refreshText()
    {
        StringBuilder txt = new StringBuilder();

        currentOrder.forEach(e ->
        {
            txt.append(e.toString() + "\n");
        });

        this.menuItemsTxt.setText(txt.toString());
    }

    private void createOrder()
    {
        this.order = restaurantProcessing.createOrder(Integer.parseInt(orderIdText.getText()), new Date(System.currentTimeMillis()), Integer.parseInt(tableIdText.getText()), this.currentOrder);
    }

    private void clearList()
    {
        currentOrder.clear();
    }

    private void addToList()
    {
        currentOrder.add(restaurantProcessing.getMenu().get(Integer.parseInt(indexText.getText())));
    }

    private void computePrice()
    {
        priceTxt.setText(Float.toString(restaurantProcessing.computePrice(order)));
    }

    private void createBill()
    {
        restaurantProcessing.generateBill(this.order);
    }
}
