package Business;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

public interface IRestaurantProcessing
{
    /**
     * @invariant menu != null && orders != null
     */


    /**
     *
     * @pre items != null
     * @pre item != null
     * @post items.size() == items.size()@pre + 1
     * @post @forall k : [0..items.size() - 2] @(items.get(k) == items.get(k)@pre)
     */
    void addMenuItem(List<MenuItem> items, MenuItem item);

    /**
     * @pre name != ""
     * @pre price >= 0.0f
     * @post @result == new BaseProduct(name, price) || @result == new CompositeProduct(name)
     */
    MenuItem createMenuItem(String name, float price, boolean type);

    /**
     * @pre items != null && items.size() > 0
     * @pre id >= 0 && id <= items.size()
     * @post items.size == items.size()@pre - 1
     */
    void deleteMenuItem(List<MenuItem> items, int id);

    /**
     * @pre items.size() > 0
     * @pre id >= 0 && id <= items.size()
     * @pre updated != null
     * @post items.size() == items.size()@pre
     */
    void editMenuItem(List<MenuItem> items, int id, MenuItem updated);

    /**
     * @pre items.size() > 0
     * @pre index >= 0 && index < items.size()
     * @pre table > 0
     * @post orders.size() == orders.size()@pre + 1
     * @post @result == new Order(index, dateOrdered, table)
     */
    Order createOrder(int index, Date dateOrdered, int table, List<MenuItem> items);

    /**
     * @pre o != null
     * @pre orders.keySet.contains(o)
     * @post @result == orders.get(o).computePrice()
     * @post @result > 0.0f
     */
    float computePrice(Order o);

    /**
     * @pre o != null
     * @pre orders.keySet.contains(o)
     * @post @nochange
     */
    void generateBill(Order o);

    /**
     *
     * @pre index == 0 || index == 1
     * @post @nochange
     */
    DefaultTableModel getTable(int index);

    /**
     *
     * @pre items != null
     * @post @nochange
     */
    DefaultTableModel childTable(List<MenuItem> items);

    /**
     * @pre true
     * @post nochange
     */
    List<MenuItem> getMenu();
}
