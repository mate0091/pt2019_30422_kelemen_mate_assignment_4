package Business;

import java.util.Date;
import java.util.Objects;

public class Order
{
    private int id;
    private Date dateOrdered;
    private int table;

    public Order(int id, Date dateOrdered, int table)
    {
        this.id = id;
        this.dateOrdered = dateOrdered;
        this.table = table;
    }

    public int getId() {
        return id;
    }

    public Date getDateOrdered() {
        return dateOrdered;
    }

    public int getTable() {
        return table;
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(id, dateOrdered, table);
    }
}
