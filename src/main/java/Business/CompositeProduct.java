package Business;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CompositeProduct extends MenuItem implements Serializable
{
    public static final long serialVersionUID = 10L;
    private ArrayList<MenuItem> childMenuItems;

    public CompositeProduct(String name)
    {
        this.name = name;
        childMenuItems = new ArrayList<MenuItem>();
    }

    @Override
    public float computePrice()
    {
        float price = 0.0f;

        for(MenuItem i : childMenuItems)
        {
            price += i.computePrice();
        }

        return price;
    }

    public void add(MenuItem menuItem, int id)
    {
        childMenuItems.add(id, menuItem);
    }

    public void remove(MenuItem obj)
    {
        childMenuItems.remove(obj);
    }

    public MenuItem getChild(int index)
    {
        return childMenuItems.get(index);
    }

    public ArrayList<MenuItem> getChildMenuItems() {
        return childMenuItems;
    }

    public String toStringTree(int iteration, boolean isRoot)
    {
        StringBuilder sb = new StringBuilder();

        sb.append(super.toStringTree(iteration, isRoot));

        childMenuItems.forEach(e -> {
                sb.append(e.toStringTree(iteration + 1, false));
        });

        return sb.toString();
    }
}
