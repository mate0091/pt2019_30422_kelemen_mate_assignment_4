package Business;

import javax.swing.table.DefaultTableModel;
import Data.FileWriter;
import Data.RestaurantSerializer;

import java.util.*;

public class Restaurant extends Observable implements IRestaurantProcessing
{
    private Map<Order, List<MenuItem>> orders;
    private List<MenuItem> menu;

    private static final Restaurant INSTANCE = new Restaurant();

    private Restaurant()
    {
        orders = new HashMap<>();

        if(RestaurantSerializer.deserialize() == null)
        {
            menu = new ArrayList<>();
        }

        else menu = RestaurantSerializer.deserialize();

        assert menu != null;
    }

    public static Restaurant getInstance()
    {
        return INSTANCE;
    }

    public MenuItem createMenuItem(String name, float price, boolean type)
    {
        assert name != "";
        assert price > 0.0f;

        MenuItem item;

        if(!type)
        {
            item = new BaseProduct(name, price);
        }

        else
        {
            item = new CompositeProduct(name);
        }

        return item;
    }

    public void addMenuItem(List<MenuItem> items, MenuItem item)
    {
        assert items != null;
        assert item != null;

        int size = items.size();

        items.add(item);

        RestaurantSerializer.serialize(this.menu);

        assert items.size() == size + 1;
    }

    public void deleteMenuItem(List<MenuItem> items, int id)
    {
        assert items.size() > 0;
        assert id >= 0 && id < items.size();

        int size = items.size();

        items.remove(id);

        RestaurantSerializer.serialize(this.menu);

        assert items.size() == size + 1;
    }

    public void editMenuItem(List<MenuItem> items, int id, MenuItem updated)
    {
        assert items.size() > 0;
        assert id >= 0 && id < items.size();

        int size = items.size();

        items.set(id, updated);

        RestaurantSerializer.serialize(this.menu);

        assert size == items.size();
    }

    public Order createOrder(int index, Date dateOrdered, int table, List<MenuItem> items)
    {
        assert table > 0;
        assert items.size() > 0;
        assert index >= 0 && index < menu.size();

        int size = orders.size();

        Order newOrder = new Order(index, dateOrdered, table);

        orders.put(newOrder, items);

        setChanged();
        notifyObservers(items);

        assert size + 1== orders.size();

        return newOrder;
    }

    public float computePrice(Order o)
    {
        assert o != null;
        assert orders.keySet().contains(o);

        float price = 0.0f;

        Iterator<MenuItem> it = orders.get(o).iterator();

        while(it.hasNext())
        {
            price += it.next().computePrice();
        }

        assert price > 0.0f;

        return price;
    }

    public void generateBill(Order o)
    {
        assert o != null;
        assert orders.keySet().contains(o);

        FileWriter.createReceipt(o, computePrice(o), orders.get(o));
    }

    public DefaultTableModel getTable(int index)
    {
        assert index == 0 || index == 1;

        List<String> columns = new ArrayList<>();
        DefaultTableModel model;

        if(index == 0) //MenuItems
        {
            columns.add("id");
            columns.add("name");
            columns.add("price");

            model = new DefaultTableModel(columns.toArray(), 0);

            //add rows
            int cnt = 0;
            for(MenuItem i : menu)
            {
                Vector<String> row = new Vector<>();

                row.add(Integer.toString(cnt));
                row.add(i.getName());
                row.add(Float.toString(i.computePrice()));

                model.addRow(row);

                cnt++;
            }
        }

        else
        {
            columns.add("id");
            columns.add("date");
            columns.add("items");
            columns.add("price");

            model = new DefaultTableModel(columns.toArray(), 0);

            //add rows

            Iterator it = orders.entrySet().iterator();

            while(it.hasNext())
            {
                Map.Entry<Order, Collection<MenuItem>> pair = (Map.Entry)it.next();

                Vector<String> row = new Vector<>();

                row.add(Integer.toString(pair.getKey().getId()));
                row.add(pair.getKey().getDateOrdered().toString());

                StringBuilder sb = new StringBuilder();

                Iterator<MenuItem> iter = pair.getValue().iterator();

                while(iter.hasNext())
                {
                    sb.append(iter.next().toString() + ", ");
                }

                row.add(sb.toString());
                row.add(Float.toString(computePrice(pair.getKey())));

                model.addRow(row);
            }
        }

        return model;
    }

    public DefaultTableModel childTable(List<MenuItem> items)
    {
        assert items.size() >= 0;
        DefaultTableModel model;

        List<String> columns = new ArrayList<>();

        columns.add("id");
        columns.add("name");
        columns.add("price");

        model = new DefaultTableModel(columns.toArray(), 0);

        //add rows
        int cnt = 0;

        for(MenuItem i : items)
        {
            Vector<String> row = new Vector<>();

            row.add(Integer.toString(cnt));
            row.add(i.getName());
            row.add(Float.toString(i.computePrice()));

            model.addRow(row);

            cnt++;
        }

        return model;
    }

    public List<MenuItem> getMenu() {
        assert menu != null;
        return menu;
    }
}
