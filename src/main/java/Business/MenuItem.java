package Business;

import java.io.Serializable;

public abstract class MenuItem implements Serializable
{
    public static final long serialVersionUID = 12L;
    protected String name;

    public abstract float computePrice();

    public String toString()
    {
        return this.name;
    }

    public String getName() {
        return name;
    }

    public String toStringTree(int iteration, boolean isRoot)
    {
        StringBuilder sb = new StringBuilder();

        if(!isRoot)
        {
            for (int i = 0; i < iteration; i++) {
                sb.append("    ");
            }
        }

        sb.append(this.name + " " + this.computePrice() + "\n");

        return sb.toString();
    }
}
