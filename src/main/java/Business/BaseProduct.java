package Business;

import java.io.Serializable;

public class BaseProduct extends MenuItem implements Serializable
{
    public static final long serialVersionUID = 11L;
    private float price;

    public BaseProduct(String name, float price)
    {
        this.name = name;
        this.price = price;
    }

    @Override
    public float computePrice()
    {
        return price;
    }
}
