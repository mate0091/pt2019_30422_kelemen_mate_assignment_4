import Business.Restaurant;
import Presentation.AdministratorGUI;
import Presentation.ChefGUI;
import Presentation.WaiterGUI;

public class Main
{
    private ChefGUI chef;
    private AdministratorGUI adminGUI;
    private WaiterGUI waiterGUI;

    public Main()
    {
        chef = new ChefGUI();
        adminGUI = new AdministratorGUI();
        waiterGUI = new WaiterGUI();
        Restaurant.getInstance().addObserver(chef);
    }

    public static void main(String[] args)
    {
        new Main();
    }
}
